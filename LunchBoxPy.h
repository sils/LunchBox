#include <boost/python.hpp>
#include "LunchBox.h"
using namespace boost::python;

BOOST_PYTHON_MODULE(LunchBox) {
    class_<Food>("Food");

    class_<Cookie, bases<Food>>("Cookie");

    class_<LunchBox>("LunchBox", init<std::vector<Food *>>())
        .def("take_food", &LunchBox::take_food);
}
