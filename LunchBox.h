#include <string>
#include <vector>

class Food {
    public:
        virtual ~Food() {};
};

class Cookie: public Food {
    public:
        virtual ~Cookie() {};
};

class LunchBox {
    public:
        LunchBox(std::vector<Food *> food);
        virtual ~LunchBox() {};

        std::string caption;
        Food *take_food();

    protected:
        std::vector<Food*> food;

    private:

};
